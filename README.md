# custom-calamares-config

This repository, based on the CARLI project, aims to facilitate the use of the custom calamares config.

## Prerequisites
Make sure Calamares (version 3.2.39 or earlier) is already installed on your system with all its dependencies (such as `mkinitcpio-openswap` and `ckbcomp`). The lastest version (3.2.40+) does not seem to work with this configuration.
Get the repo `git clone https://gitlab.com/F4IEY/custom-calamares-config` and place its contents into your future file system (`/etc/calamares`) for further modifications.

## Customizing
This configuration contains several files that are interesting for DIY customization:
1) Branding
    * `branding.desc` for the main description of Calamares window
    * `show.qml` to edit the displayed image slideshow during the installation
2) Modules
    * `initcpio.conf` to initialise the used kernel
    * `netinstall-*.conf` comes with associated `netinstall-*.yaml` to describe a netinstall package list (a.k.a *Package Selection* tab)
    * `shellprocess-*.conf` to use shell commands before, during or after the installation
    * `removeuser.conf` has to contain the right username for the live session you want to delete
3) Main settings
    * `settings.conf` contains the name of used instances (netinstall and shellprocess)

## Main Section
First go into `/etc/calamares` and take a look at `settings.conf`. Some lines should look like below:
```yaml
# YAML: list of maps of string:string key-value pairs.
instances:
- id:       software
  module:   netinstall
  config:   netinstall-software.conf
- id:       techxero
  module:   netinstall
  config:   netinstall-techxero.conf  
- id:       carlibefore
  module:   shellprocess
  config:   shellprocess-carlibefore.conf
- id:       carliafter
  module:   shellprocess
  config:   shellprocess-carliafter.conf
```
This is where the custom modules are declared. To work, the declared configuration files *have to be in* `/etc/calamares/modules`.
These instances are then initialized in the main sequence a few lines below like this:<br />
*netinstall*
```yaml
sequence:
- show:
  - welcome
#  - notesqml
  - netinstall@software
  - netinstall@techxero  
  - locale
  - keyboard
  - partition
  - users
...
```
*shellprocess*
```yaml
- exec:
#  - dummycpp
#  - dummyprocess
#  - dummypython
  - partition
  - mount
  - unpackfs
  ...  
  - shellprocess@carlibefore
  ...
  - shellprocess@carliafter
  ...
```
Feel free to add or remove custom modules. They just have to be described here and declared in `/etc/calamares/modules`

## Modules Section
### Custom netinstall/shellprocess modules
In `/etc/calamares/modules` you need then to edit the netinstall and shellprocess files. In the *netinstall* configuration files, specify the net and/or local path you want to. By default, it should look like this:
```yaml
# Net-based package list, with fallback to local file
groupsUrl:
  - https://raw.githubusercontent.com/arcolinuxiso/carli-calamares-config/master/etc/calamares/modules/netinstall-software.yaml
  - file:///etc/calamares/modules/netinstall-software.yaml

```
For the *netinstall*, you can edit `netinstall-*.yaml` to choose which packages you want on the *Package Selection* tab.


For the *shellprocess* (e.g in `shellprocess-carlibefore.conf`) configuration files, you can find it here (last lines):
```yaml
script:
    - command: "/path/to/you/sh/file.sh"
```
You can also custom the displayed text when the module is enabled:
```yaml
i18n:
     name: "Preparing your system... Please wait"
```
### Initcpio
`initcpio.conf` is the configuration file where you specify the kernel you want to load, e.g for linux LTS kernel:
```yaml
kernel: linux-lts
```
### Removeuser
`removeuser.conf` is the configuration file where you specify the user name you want to delete after completing the installation, if the name is `liveuser`:
```yaml
username: liveuser
```
### Packages
You can find the package configuration into `packages.conf`. This is where you specify your package manager (here, we use `pacman` for Arch Linux but it can be `aptitude` for Debian, `portage` for Gentoo...)
You can also add other packages in the `operations` list to be removed after the installation.

### Unpackfs
In the configuration file `unpackfs.conf`, if you're not using archiso, you'll maybe need to edit these paths to match your system:
```yaml
unpack:
    -   source: "/path/to/your/filesystem/x86_64/airootfs.sfs"
        sourcefs: "squashfs"
        destination: ""
    -   source: "/path/to/your/kernel/boot/x86_64/vmlinuz-linux"
        sourcefs: "file"
        destination: "/boot/vmlinuz-linux"
```
## Branding Section
### Branding description
In `/etc/calamares/branding/default`, you will find the `branding.desc` file. This file customizes the welcome page thanks to this section: 
```yaml
# The *Url* entries are used on the welcome page, and they
# are visible as buttons there if the corresponding *show* keys
# are set to "true" (they can also be overridden).
strings:
    productName:         MY OS
    shortProductName:    Generic
    version:             2022.6.9
    shortVersion:        2021.1
    versionedName:       MY OS 'v1'
    #shortVersionedName:  Generic 2017.8
    bootloaderEntryName: Generic
    productUrl:          https://calamares.io/
    supportUrl:          https://github.com/calamares/calamares/issues
    knownIssuesUrl:      https://calamares.io/about/
    releaseNotesUrl:     https://calamares.io/about/
    donateUrl:           https://kde.org/community/donations/index.php
```
You can also define your own custom images and icons here:
```yaml
# These filenames can also use substitutions from os-release (see above).
images:
    # productBanner:       "banner.png"
    productIcon:         "squid.png"
    productLogo:         "squid.png"
    # productWallpaper:    "wallpaper.png"
    productWelcome:      "languages.jpg"
```
Or just replace the images with the same name.

### Customize the installation slideshow
If you want to customize even more this part, it is possible to do so by editing the `show.qml` file. It contains the image sequence, their label and names. Edit the picture source name and the label as you want to:
```yml
    Image {
        id: background1
        source: "welcometo.png" #image source
        width: parent.width; height: parent.height
        horizontalAlignment: Image.AlignCenter
        verticalAlignment: Image.AlignTop
        fillMode: Image.Stretch
        anchors.fill: parent
    	}

    Text {
        anchors.horizontalCenter: background.horizontalCenter
        anchors.top: background.bottom
        text: "Welcome to" #image label
        wrapMode: Text.WordWrap
        width: presentation.width
        horizontalAlignment: Text.Center
    	}
    }
```
